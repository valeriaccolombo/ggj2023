using UnityEngine;

public class TutorialScreen : MainMenuScreen
{
    [SerializeField] private GameObject page1;
    [SerializeField] private GameObject page2;

    private void Start()
    {
        GoToPage1();
    }

    public void GoToPage1()
    {
        page1.SetActive(true);
        page2.SetActive(false);
    }

    public void GoToPage2()
    {
        page1.SetActive(false);
        page2.SetActive(true);
    }
}
