using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResultsView : MonoBehaviour
{
    public void OnHomeClick()
    {
        StartCoroutine(WaitALittleAndThenClose());
    }

    private IEnumerator WaitALittleAndThenClose()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("MainMenuScene");
    }
}
