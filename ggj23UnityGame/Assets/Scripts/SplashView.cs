using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashView : MonoBehaviour
{
    public void Start()
    {
        StartCoroutine(WaitAndLoadGame());
    }

    private IEnumerator WaitAndLoadGame()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("MainMenuScene");
    }
}
