﻿using System.Collections;
using UnityEngine;

public class MainMenuScreen : MonoBehaviour
{
    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }

    public void OnBtnCloseClick()
    {
        StartCoroutine(WaitALittleAndThenClose());
    }

    private IEnumerator WaitALittleAndThenClose()
    {
        yield return new WaitForSeconds(0.5f);
        Hide();
    }
}
