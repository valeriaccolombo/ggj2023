using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuView : MonoBehaviour
{
    [SerializeField] private CreditsScreen credits;
    [SerializeField] private TutorialScreen tutorial;

    public void Start()
    {
        credits.Hide();
        tutorial.Hide();
    }

    public void OnTutorialBtnClick()
    {
        tutorial.Show();
    }

    public void OnCreditsBtnClick()
    {
        credits.Show();
    }

    public void OnPlayBtnClick()
    {
        SceneManager.LoadScene("GameScene");
    }
}

