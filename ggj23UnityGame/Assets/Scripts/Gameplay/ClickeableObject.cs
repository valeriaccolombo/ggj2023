﻿using System;
using UnityEngine;

public class ClickeableObject : MonoBehaviour
{
    protected bool isSelected {get; private set;}

    public void Select()
    {
        isSelected = true;
    }

    internal void UnSelect()
    {
        isSelected = false;
    }
}

