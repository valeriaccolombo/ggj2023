public class InfiniteWaterSource : WaterSource
{
    public override float GetEnergy()
    {
        return ConsumeEnergy;
    }
}
