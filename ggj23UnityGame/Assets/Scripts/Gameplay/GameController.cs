using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public int CurrentLevel = 1;

    [SerializeField] private Transform CameraTransform;
    [SerializeField] private Slider SliderSun;
    [SerializeField] private Slider SliderWater;
    [SerializeField] private DayLoop DayLoopController;
    [SerializeField] private float CamMoveSpeed = 1.5f;

    [SerializeField] private SpriteRenderer[] Leaves;
    [SerializeField] private Color HealthyLeaveColor;
    [SerializeField] private Color UnhealthyLeaveColor;

    [SerializeField] private GameObject[] TreeByLvl;
    [SerializeField] private GameObject[] RootsByLvl;

    [SerializeField] private float[] StartingSunLevelByLvl;// = 50;
    [SerializeField] private float[] StartingWaterLevelByLvl;// = 50;
    [SerializeField] private float[] MaxSunLevelByLvl;// = 100;
    [SerializeField] private float[] MaxWaterLevelByLvl;// = 100;
    [SerializeField] private float[] SunDecreaseSpeedByLvl;// = 0.5f;
    [SerializeField] private float[] WaterDecreaseSpeedByLvl;// = 1;
    [SerializeField] private float[] NightWaterConsumeMultiplicatorByLvl;// = 0.5f;
    [SerializeField] private float[] NightSunConsumeMultiplicatorByLvl;// = 1.5f;

    [SerializeField] private float[] CameraStartPositionByLvl;
    [SerializeField] private float[] CameraMaxPositionByLvl;
    [SerializeField] private float[] CameraZoomByLvl;

    private bool CamUp;
    private bool CamDown;

    private float CurrentSunLevel;
    private float CurrentWaterLevel;

    public void Start()
    {
        StartANewLevel();

        StartCoroutine(ConsumeWater());
        StartCoroutine(ConsumeSun());
    }

    private void StartANewLevel()
    {
        DayLoopController.GoToLevel(CurrentLevel);

        SliderSun.minValue = 0;
        SliderWater.minValue = 0;
        SliderSun.maxValue = MaxSunLevelByLvl[CurrentLevel - 1];
        SliderWater.maxValue = MaxWaterLevelByLvl[CurrentLevel - 1];

        CurrentWaterLevel = StartingWaterLevelByLvl[CurrentLevel - 1];
        CurrentSunLevel = StartingSunLevelByLvl[CurrentLevel - 1];

        CameraTransform.GetComponent<Camera>().orthographicSize = CameraZoomByLvl[CurrentLevel - 1];
        CameraTransform.localPosition = new Vector3(0, CameraStartPositionByLvl[CurrentLevel - 1], -30);

        for (int i = 0; i < TreeByLvl.Length; i++)
        {
            RootsByLvl[i].SetActive(CurrentLevel >= i + 1);
            TreeByLvl[i].SetActive(CurrentLevel == i + 1);
        }

        reachedToLevelUp = false;
    }

    private IEnumerator ConsumeWater()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (!reachedToLevelUp)
            {
                var mult = DayLoopController.IsDay ? 1 : NightWaterConsumeMultiplicatorByLvl[CurrentLevel - 1];
                CurrentWaterLevel = Math.Max(0, CurrentWaterLevel - (WaterDecreaseSpeedByLvl[CurrentLevel - 1] * mult));
                CheckIfLvelUpOrLost();
            }
        }
    }

    public void AbsorbWater(float amount)
    {
        CurrentWaterLevel = Math.Min(MaxWaterLevelByLvl[CurrentLevel - 1], CurrentWaterLevel + amount);
        CheckIfLvelUpOrLost();
    }

    private IEnumerator ConsumeSun()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);

            if (!reachedToLevelUp)
            {
                var mult = DayLoopController.IsDay ? 1 : NightSunConsumeMultiplicatorByLvl[CurrentLevel - 1];
                CurrentSunLevel = Math.Max(0, CurrentSunLevel - (SunDecreaseSpeedByLvl[CurrentLevel - 1] * mult));
                CheckIfLvelUpOrLost();
            }
        }
    }

    public void AbsorbSun(float amount)
    {
        CurrentSunLevel = Math.Min(MaxSunLevelByLvl[CurrentLevel - 1], CurrentSunLevel + amount);
        CheckIfLvelUpOrLost();
    }

    public void FixedUpdate()
    {
        SliderSun.value = CurrentSunLevel;
        SliderWater.value = CurrentWaterLevel;

        var isUnhealthy = CurrentWaterLevel < MaxWaterLevelByLvl[CurrentLevel - 1] * 0.3f || CurrentSunLevel < MaxSunLevelByLvl[CurrentLevel - 1] * 0.3f;
        foreach (var leave in Leaves)
            leave.color = isUnhealthy ? UnhealthyLeaveColor : HealthyLeaveColor;

        if(CamUp)
        {
            OnCamUpBtnClick();

        }
        else if(CamDown)
        {
            OnCamDownBtnClick();
        }
    }

    public void OnCloseBtnClick()
    {
        StartCoroutine(WaitALittleAndThenClose());
    }

    private IEnumerator WaitALittleAndThenClose()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("MainMenuScene");
    }

    public void OnCamDownBtnClick()
    {
        if (CameraTransform.localPosition.y > -52)
            CameraTransform.Translate(0, -CamMoveSpeed, 0);
    }

    public void OnCamUpBtnClick()
    {
        if (CameraTransform.localPosition.y < CameraMaxPositionByLvl[CurrentLevel - 1])
            CameraTransform.Translate(0, CamMoveSpeed, 0);
    }

    public void OnCamUpBtnDown()
    {
      //  CamUp = true;
    }
    public void OnCamUpBtnUp()
    {
        CamUp = false;
    }

    public void OnCamDownBtnDown()
    {
    //    CamDown = true;
    }

    public void OnCamDownBtnUp()
    {
        CamDown = false;
    }
    
    private bool reachedToLevelUp = false;
    public void CheckIfLvelUpOrLost()
    {
        reachedToLevelUp = CheckWinCondition();
        if (reachedToLevelUp)
            Debug.Log("Level up! Starts with next day");

        if (CurrentWaterLevel == 0 || CurrentSunLevel == 0)
        {
            SceneManager.LoadScene("ResultsSceneLOST");
        }
    }

    private bool CheckWinCondition()
    {
        return CurrentWaterLevel == MaxWaterLevelByLvl[CurrentLevel - 1] && CurrentSunLevel == MaxSunLevelByLvl[CurrentLevel - 1];
    }

    public void LevelUp()
    {
        if(reachedToLevelUp)
        {
            CurrentLevel++;
            if(CurrentLevel > 3)
            {
                SceneManager.LoadScene("ResultsSceneWIN");
            }
            else
            {
                StartANewLevel();
            }
        }
    }
}
