using UnityEngine;
using UnityEngine.UI;

public class FixedSteeringWheel : SteeringWheel
{
    public Graphic Fill;

    protected override void ReturnWheelToStartingPosition()
    {
        //do nothing
    }

    protected override void ReleasingExtraActions()
    {
        Fill.rectTransform.localEulerAngles += Vector3.back * wheelAngle;
        wheelAngle = 0f;
    }
}