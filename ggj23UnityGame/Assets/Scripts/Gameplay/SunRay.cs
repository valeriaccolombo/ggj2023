using UnityEngine;

public class SunRay : MonoBehaviour
{
    [SerializeField] private float speed = 1;
    [SerializeField] private float Energy = 0.1f;

    private Vector3 targetPosition;
    private SpriteRenderer sprite;

    private void Start()
    {
        targetPosition = transform.localPosition;
        transform.localPosition = Vector3.zero;

        sprite = GetComponent<SpriteRenderer>();
    }

    public float AbsorbEnergy()
    {
        if (sprite.enabled)
        {
            sprite.enabled = false;
            return Energy;
        }
        else
        {
            return 0;
        }
    }

    private void FixedUpdate()
    {
        var currentPosition = transform.localPosition;

        var isXDone = false;
        var translateX = (targetPosition.x - currentPosition.x);
        if(Mathf.Abs(translateX) > 0.001f)
        {
            translateX = translateX / 100 * speed;
        }
        else
        {
            isXDone = true;
        }

        var isYDone = false;
        var translateY = (targetPosition.y - currentPosition.y);
        if (Mathf.Abs(translateY) > 0.001f)
        {
            translateY = translateY / 100 * speed;
        }
        else
        {
            isYDone = true;
        }

        transform.Translate(translateX, translateY, 0);

        if (isXDone && isYDone)
        {
            transform.localPosition = Vector3.zero;
            sprite.enabled = true;
        }
    }
}
