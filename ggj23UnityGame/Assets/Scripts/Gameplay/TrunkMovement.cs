using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrunkMovement : ClickeableObject
{
    [SerializeField] private GameController Game;
    [SerializeField] private SteeringWheel steeringWheel;
    [SerializeField] private float MaxZRot = 1;
    [SerializeField] private float MinZRot = 0;
    [SerializeField] private TrunkCollider[] Colliders;

    public float speed = 0.2f;

    private bool started = false;

    public void FixedUpdate()
    {
        if (!started)
        {
            started = true;
        }

        ReactToWheelControls();

        foreach(var coll in Colliders)
        {
            Game.AbsorbSun(coll.SunAbsorbed());
        }
    }

    private void ReactToWheelControls()
    {
        var angleToRotate = -steeringWheel.GetClampedValue() * speed * Time.deltaTime;

        if(angleToRotate != 0)
        {
            var rot = transform.localRotation;

            if (((rot.z <= MinZRot - 0.1f) || rot.z >= MaxZRot) && angleToRotate > 0)
            {
                rot.z = MaxZRot;
                transform.localRotation = rot;
                return;
            }
            if (rot.z <= MinZRot && angleToRotate < 0)
            {
                rot.z = MinZRot;
                transform.localRotation = rot;
                return;
            }

            transform.Rotate(0, 0, -steeringWheel.GetClampedValue() * speed * Time.deltaTime);
        }
    }
}
