using UnityEngine;

public class WaterSource : MonoBehaviour
{
    [SerializeField] private float TotalEnergy = 15;
    [SerializeField] protected float ConsumeEnergy = 0.1f;

    [SerializeField] protected SpriteRenderer CapacityView;

    private float initialEnergy;

    private void Start()
    {
        initialEnergy = TotalEnergy;
    }

    public virtual float GetEnergy()
    {
        if (TotalEnergy >= 0)
        {
            TotalEnergy -= ConsumeEnergy;

            var color = CapacityView.color;
            color.a = TotalEnergy / initialEnergy;
            CapacityView.color = color;

            return ConsumeEnergy;
        }
        return 0;
    }
}
