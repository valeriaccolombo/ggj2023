
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootMovement : ClickeableObject
{
    [SerializeField] private GameController Game;
    [SerializeField] private FixedSteeringWheel steeringWheel;

    public float speed = 0.2f;
    public SpriteRenderer pointAsset;
    private List<WaterSource> collidingWaters = new List<WaterSource>();

    private bool started = false;

    public void FixedUpdate()
    {
        if (!started)
        {
            started = true;
            StartCoroutine(AutonomousMovement());
            steeringWheel.OnRelease.AddListener(OnSteeringWheelReleased);
        }

        foreach (var pond in collidingWaters)
        {
            Game.AbsorbWater(pond.GetEnergy());
        }
    }

    [SerializeField] private float changeRotationFrequency = 500;
    [SerializeField] private float chanceGoingLeft = 0.3f;
    [SerializeField] private float chanceGOingRight = 0.3f;
    [SerializeField] private float chanceGoingDown = 0.6f;
    [SerializeField] private float chanceGoingUp = 0.1f;

    private Quaternion currentDirection;

    private IEnumerator AutonomousMovement()
    {
        bool pointing_left = false;
        bool pointing_right = false;
        bool pointing_up = false;
        bool pointing_down = false;

        while (true)
        {
            var rand_horizontal_move = Random.Range(0f, 1f);
            var rand_vertical_move = Random.Range(0f, 1f);
            pointing_left = rand_horizontal_move < chanceGoingLeft;
            pointing_right = rand_horizontal_move > 1-chanceGOingRight;
            pointing_down = rand_vertical_move < chanceGoingDown;
            pointing_up = rand_vertical_move > 1-chanceGoingUp;

            var eulerZAngle = CalculateEulerZAngle(pointing_left, pointing_right, pointing_up, pointing_down);
            currentDirection = Quaternion.Euler(0, 0, eulerZAngle);

            for (var i = 0; i < changeRotationFrequency; i++)
            {
                yield return new WaitForEndOfFrame();
                Move();
            }
        }
    }

    private void OnSteeringWheelReleased(float rotationWhenReleasingTheWheel)
    {
        currentDirection.z += rotationWhenReleasingTheWheel;
    }

    private float CalculateEulerZAngle(bool pointing_left, bool pointing_right, bool pointing_up, bool pointing_down)
    {
        var eulerZAngle = 0;
        if (pointing_up)
        {
            if (pointing_left)
            {
                //  Debug.Log("NO");
                eulerZAngle = 135;
            }
            else if (pointing_right)
            {
                //   Debug.Log("NE");
                eulerZAngle = 45;
            }
            else
            {
                //   Debug.Log("N");
                eulerZAngle = 90;
            }
        }
        else if (pointing_down)
        {
            if (pointing_left)
            {
                //   Debug.Log("SO");
                eulerZAngle = 225;

            }
            else if (pointing_right)
            {
                //   Debug.Log("SE");
                eulerZAngle = 315;
            }
            else
            {
                //   Debug.Log("S");
                eulerZAngle = 270;
            }
        }
        else
        {
            if (pointing_left)
            {
                //   Debug.Log("O");
                eulerZAngle = 180;

            }
            else if (pointing_right)
            {
                //  Debug.Log("E");
                eulerZAngle = 0;
            }
        }
        return eulerZAngle;
    }

    private void Move()
    {
        var modifiedByUserRot = currentDirection;
        modifiedByUserRot.z += steeringWheel.GetClampedValue();

        transform.rotation = modifiedByUserRot;

        transform.Translate(speed * Time.deltaTime, 0, 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "water")
        {
            //Debug.Log("tasty water");
            var pond = collision.GetComponent<WaterSource>();
            collidingWaters.Add(pond);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var pond = collision.GetComponent<WaterSource>();
        if (collidingWaters.Contains(pond))
        {
            collidingWaters.Remove(pond);
        }
    }

}
