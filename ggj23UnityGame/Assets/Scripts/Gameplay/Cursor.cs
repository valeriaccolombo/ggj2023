using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursor : MonoBehaviour
{
    [SerializeField] private Camera camera;
    private ClickeableObject objectUnderCursor;
    private ClickeableObject lastSelectedObject;

    private void Update()
    {
        UpdatePosition();

        if (Input.GetMouseButtonDown(0))
        {
            StartCoroutine(TryClick());
        }
    }

    private IEnumerator TryClick()
    {
        UpdatePosition();

        yield return new WaitForSeconds(0.05f);

        if (objectUnderCursor != null)
        {
            if (lastSelectedObject != null)
            {
                lastSelectedObject.UnSelect();
            }

            //Debug.Log("Click sobre " + objectUnderCursor.tag);
            objectUnderCursor.Select();
            lastSelectedObject = objectUnderCursor;
        }
        else
        {
            //Debug.Log("No hay nada bajo el cursor");
        }
    }

    private void UpdatePosition()
    {
        var mousePosition = camera.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);
        mousePosition.z = 0;
        transform.position = mousePosition;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "root" || collision.tag == "trunk")
        {
            //Debug.Log("Entrando a " + collision.tag);
            objectUnderCursor = collision.GetComponent<ClickeableObject>();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "root" || collision.tag == "trunk")
        {
            //Debug.Log("Saliendo de " + collision.tag);
            objectUnderCursor = null;
        }
    }
}
