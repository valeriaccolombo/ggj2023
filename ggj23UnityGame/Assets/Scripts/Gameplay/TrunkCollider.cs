﻿using System;
using System.Collections.Generic;
using UnityEngine;

class TrunkCollider : MonoBehaviour
{
    private List<SunRay> collidingRays = new List<SunRay>();

    internal float SunAbsorbed()
    {
        var totalAbsorbed = 0f;
        foreach (var ray in collidingRays)
        {
            totalAbsorbed += ray.AbsorbEnergy();
        }
        return totalAbsorbed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "sun")
        {
            var ray = collision.GetComponent<SunRay>();
            if (!collidingRays.Contains(ray))
            {
                collidingRays.Add(ray);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "sun")
        {
            var ray = collision.GetComponent<SunRay>();
            if (collidingRays.Contains(ray))
            {
                collidingRays.Remove(ray);
            }
        }
    }
}
