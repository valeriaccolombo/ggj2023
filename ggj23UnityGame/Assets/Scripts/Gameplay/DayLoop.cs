using UnityEngine;

public class DayLoop : MonoBehaviour
{
    [SerializeField] private GameController Game;

    public bool IsDay;

    public void DayStarts()
    {
        IsDay = true;
    }

    public void NightStarts()
    {
        IsDay = false;
    }

    public void NewDayBegins()
    {
        Game.LevelUp();
    }

    public void GoToLevel(int lvl)
    {
//        GetComponent<Animator>().speed = 5;
        GetComponent<Animator>().Play("dayloop_lvl" + lvl);
    }
}
