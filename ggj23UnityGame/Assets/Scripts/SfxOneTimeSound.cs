using UnityEngine;

public class SfxOneTimeSound : MonoBehaviour
{
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = false;
    }

    public void PlaySfx()
    {
        audioSource.Play();
    }
}
